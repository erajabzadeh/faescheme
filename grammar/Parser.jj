PARSER_BEGIN(Parser)
package parser;

import ast.*;

public class Parser {}

PARSER_END(Parser)

SKIP:
{
    " "
  | "\t"
  | "\n"
  | "\r"
  | <";" (~["\n","\r"])* ("\n"|"\r")>
}

TOKEN:
{
      <LPAR    : "(">
    | <RPAR    : ")">
    | <INTEGER : ("-")? (["0"-"9"])+>
    | <FLOAT   : ("-")? (["0"-"9"])* "." (["0"-"9"])+>
    | <LAMBDA  : "lambda">
    | <LET     : "let">
    | <ADD     : "+">
    | <SUB     : "-">
    | <MUL     : "*">
    | <DIV     : "/">
    | <SYMBOL  : (["a"-"z", "A"-"Z"]) (~["(", ")", " ", "\t", "\n", "\r"])*>
}

TOKEN:
{
    <ERROR : ~[]>
}

ASTNode begin():
{
    ASTNode root;
}
{
    (
        root = atom()
    |   root = list()
    )
    {
        return root;
    }
}
    
ASTNode atom():
{
    Token token;
    ASTNode node;
}
{
    (
      token = <INTEGER> { node = new NumberNode(Integer.parseInt(token.image)); }
    | token = <FLOAT>   { node = new NumberNode(Double.parseDouble(token.image)); }
    | token = <SYMBOL>  { node = new SymbolNode(token.image); }
    )
    {
        return node;
    }
}

ASTNode list():
{
    ASTNode  node = null;
}
{ 
    (
    (
        <LPAR>
        (
              node = app() 
            | node = add()
            | node = sub()
            | node = mul()
            | node = div()
            | node = let()
            | node = lambda() 
        )
        <RPAR>
    )
    | <EOF>
    )
  { return node; }
}

ASTNode app():
{
    ASTNode node = new ListNode(),
            f,
            a;
}
{
    (
        (
          f = list()
        | f = atom()
        )
        {
            node.addChild(f); 
        }
        (
            a = list() 
          | a = atom()
        )
        {
            node.addChild(a);
        }
    )
    {
        return node;
    }
}

ASTNode add():
{
    ASTNode node,
            lhs,
            rhs;
}
{
    <ADD>
    (
    (
      lhs = list()
    | lhs = atom()
    )
    (
      rhs = list()
    | rhs = atom()
    )
    )
    {
        return new AdditionNode(lhs, rhs);
    }
}

ASTNode sub():
{
    ASTNode node,
            lhs,
            rhs;
}
{
    <SUB>
    (
    (
      lhs = list()
    | lhs = atom()
    )
    (
      rhs = list()
    | rhs = atom()
    )
    )
    {
        return new SubtractionNode(lhs, rhs);
    }
}

ASTNode mul():
{
    ASTNode node,
            lhs,
            rhs;
}
{
    <MUL>
    (
    (
      lhs = list()
    | lhs = atom()
    )
    (
      rhs = list()
    | rhs = atom()
    )
    )
    {
        return new MultiplicationNode(lhs, rhs);
    }
}

ASTNode div():
{
    ASTNode node,
            lhs,
            rhs;
}
{
    <DIV>
    (
    (
      lhs = list()
    | lhs = atom()
    )
    (
      rhs = list()
    | rhs = atom()
    )
    )
    {
        return new DivisionNode(lhs, rhs);
    }
}

ASTNode let():
{
    ASTNode node,
            bindings = new ListNode(),
            body,
            t;
}
{
    <LET>
    <LPAR>
    (
        t = binding() { bindings.addChild(t); }
    )*
    <RPAR>
    (
        body = list()
      | body = atom()
    )
    {
        return new LetNode(bindings, body);
    }
}

ASTNode binding():
{
    ASTNode node = new ListNode(),
            expr;
    Token   token;
}
{
    <LPAR>
    token = <SYMBOL> { node.addChild(new SymbolNode(token.image)); }
    (
        expr = list()
      | expr = atom()
    )
    {
        node.addChild(expr);
    }
    <RPAR>
    {
        return node;
    }
}

ASTNode lambda():
{
    ASTNode t,
            params,
            body;
}
{
    <LAMBDA>
    <LPAR>params  = atom()<RPAR>
    (
        body = list()
      | body = atom()
    )
    {
        return new LambdaNode(params, body);
    }
}
